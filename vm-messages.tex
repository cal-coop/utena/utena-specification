\chapter{Instructions}

\section{The message send meta-medium}

Fundamentally all that is required for a message oriented object system is a protocol to send messages to an object \cite{smalltalk-design}.
In Utena a message send is composed of two stages, first a message lookup, which uses a \gls{message-selector} to find a matching
slot on an object and then message evaluation, where the machine implementation uses the object and associated slot to produce
the behaviour of the message (this distinction is also implied to exist in the Self VM \cite{self-handbook}).
This is a useful distinction to make as we can dispatch on the slot description received from \emph{message lookup}
in the machine to control the behaviour of message evaluation.

\section{Machine state}

The \gls{virtual-machine} is modelled after the SECD machine \cite{secd}.
The machine has five registers, of which the first four are similar to
the registers of the SECD machine, but are renamed to more
contemporary names:

\newcommand{\compare}[1]{similar to the #1 of the SECD machine}
\begin{itemize}
\item \emph{Instructions}: a list of remaining instructions to execute,
  \compare{\emph{control}}.
\item \emph{Environment}: an object which is the target of implicit sends,
  \compare{\emph{environment}}.
\item \emph{Data}: a stack used to store objects temporarily used by
  a method, \compare{\emph{stack} (or sometimes \emph{store})}.
\item \emph{Return}: another state which the machine returns to, after
  it finishes evaluating the current method, \compare{\emph{dump}}.
\item \emph{Dynamic environment}: an object containing a mapping
  of dynamically-bound keys to values.
\end{itemize}

\section{Instruction list} \label{section:messages:instructions}

In order to cater for \emph{message-based programming} (section
\ref{section:introduction:message-based-programming}) and keep
byte-code programs uncoupled from the interfaces of primitives Utena
provides six core instructions. Much like the Self VM, the way the
methods are expressed once they have been parsed is concerned only
with enabling one fundamental semantic of Self programs, which is a
message send to an object \cite{chambers89:self-implementation}.

Please note that some of these instructions are exactly the same as
those described in \cite{chambers89:self-implementation}, however some
are omitted as they are only relevant to the prototype based object
model present in Self.\footnote{This includes a bytecode operation to
  access the current activation record, which instead should be
  provided through the use of a \acrshort{vm} mirror.}

%% Trying to fix indentation in this list is really annyoing idk
%% https://tex.stackexchange.com/questions/174808/how-to-indent-multiline-item-within-enumeration-after-new-paragraph

Instructions currently do not have a defined encoding; they were
initially going to be encoded as \emph{bytecode}, but the instructions
can be made simpler if we don't attempt to turn them into strings
of bytes. Bytecodes are often used for \emph{serialisation} and
\emph{interpretation}. We would prefer to wait until interesting
programs have been written with Utena, to determine what we should
prefer in a serialisation format. We also suspect that an interpreter
should work on a different representation of Utena programs.

\newcommand{\instruction}[1]{\emph{#1}}
\begin{itemize}

\item \instruction{Literal} (value): Push the literal value onto the Data stack.

\item \instruction{Drop}: Remove the most recently pushed value from the
  Data stack. This instruction is useful for side effects, where a
  method sends a message without using the result returned, before
  the method sends another message.

\item \instruction{Targeted Send} (argument-count, flags):
  Pop the arguments off the Data stack, then pop the receiver and
  \gls{message-selector}, and perform a message send. The argument
  count only counts the actual arguments for the message, which does
  not include the message selector or the target object.
  
  When the Apply flag is set, an additional \emph{sequence} must be
  provided. The elements of the sequence will be used as additional
  arguments appearing after the arguments provided normally. When the
  Tail call flag is set, the current activation record is replaced
  with a new record which will return to the previous record, rather
  than returning to the current record. A sequence is any object which
  responds to the messages \texttt{length} and \texttt{at}; the
  sequence length is taken to be the the value returned from sending
  \texttt{length}, and the sequence elements the values returned from
  repeatedly sending \texttt{at} with all integers from 0 below the
  length.

\item \emph{Implicit Send} (argument-count, flags):
  With the exception that no receiver is popped from the Store,
  Arguments are read off the Data stack as per the Send instruction,
  except that no receiver is popped from the Data stack, as the
  receiver is the Environment.
  
  Flags are handled as per the Targeted Send instruction.

\item \emph{Super Send} (name, argument-count, flags):
  Find an ``outer'' object by looking for a self method with the
  given name, and send a message to that object, looking for slots
  starting at the superclass of that object.

  Flags are handled as per the Targeted Send instruction.
  
\item \emph{Outer Send} (name, argument-count, flags):

  Find an ``outer'' object by looking for a self method with the
  given name, and send a message to that object.

  Flags are handled as per the Targeted Send instruction.
\end{itemize}

\subsection{Instruction validation}
\label{section:messages:validation}

A method can only be accepted by the virtual machine if it is valid. For
a method to be valid, each instruction in the method must not cause
the Data stack to underflow, and tail-calls can only be
performed at the end of a method (i.e. in \emph{tail position}).

Validation follows an \emph{abstract interpretation} of execution of a
method, which counts the number of elements on the Data stack that each
instruction in a method produces. The abstract interpreter counts the
number of elements on the Data stack, which is initially zero
at the start of a method.. Then the abstract interpreter
processes each instruction, by subtracting the number of inputs of the
instruction from the Data stack and then adding the number of outputs
of the instruction to the count. Validation fails if the subtraction would
cause there to be a negative count of elements on the Data stack, and
validation succeeds only when the abstract interpreter finishes with
exactly one element on the Data stack. The inputs and outputs of
each instruction are summarised in Table~\ref{table:validation},
and are formalised in Section~\ref{section:semantics:validation}.

\begin{table}[h]
\begin{tabular}{| l c c l |}
  \hline
  Instruction & Inputs & Outputs & Additional requirements \\
  \hline
  Literal & 0 & 1 & \\
  Drop & 1 & 0 & \\
  Targeted Send ($n$) & $n + 2$ & 1 & Tail-call must be last instruction in method \\
  Implicit Send ($n$) & $n + 1$ & 1 & Tail-call must be last instruction in method \\
  Super Send ($n$) & $n + 1$ & 1 & Tail-call must be last instruction in method \\
  Outer Send ($n$) & $n + 1$ & 1 & Tail-call must be last instruction in method \\
  \hline
\end{tabular}
\caption{Validation requirements of each instruction.}
\label{table:validation}
\end{table}
