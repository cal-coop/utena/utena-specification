\section{\texttt{eq?} and immutable objects}

\subsection{Status}

We're totally winging it, providing an unreliable \texttt{eq?} on
immutable objects, for the sake of performance hacks.

\subsection{Context}

Many languages with mutable objects include both concepts of
\emph{structural} equality and \emph{reference} equality.  Such
languages, with both mutable and immutable objects, thus need to
define how reference equality behaves on immutable objects. For
example, Common Lisp provides both an \texttt{eq} predicate which
behaves unreliably for its immutable objects (numbers and characters),
and an \texttt{eql} predicate which tests structural equality for
immutable objects, but tests reference equality for mutable objects.

Utena exposes a \texttt{eq?} message which is intended to be used to
distinguish between objects by reference equality, but it is not clear
what should be done with immutable objects.

Henry Baker argues in \cite{equal-rights} that immutable objects
cannot be mutated in a way to tell objects apart in order to reveal
the \emph{identity} of said objects, and objects with the same
structure behave interchangeably with non-mutating operations, so all
structurally equal immutable objects should share identity. By this
argument, \texttt{eq?}  should perform structural comparisons on
immutable objects, like \texttt{eql} in Common Lisp.

However, Utena is emphatically an object-oriented system, and
structural equality of objects is still too conservative to be the
default equality test. For example, two different representations of a
set may be interchangeable, but they may not be structurally equal
\cite{left-hand-of-equals}.  Furthermore, structural equality
necessarily operates on implementation details, comparing slots of
objects.

While unreliable, using \texttt{eq?} on immutable objects can be a
useful performance hack, as (to our knowledge) reference equality
implies all other reasonable equality predicates; it would otherwise
be the case that $a \neq a$ for all $a$. We also want the user to be
able to specify their own equality predicates, rather than
have one specially provided by the implementation (as in many
ML-family languages\footnote{Note that OCaml only provides mutable
  objects.}), so such hacks cannot be internal to an implementation.

Cliff Click mentioned in a private discussion that reference equality
was necessary when using hash-consed and cyclic objects. However,
producing a cycle (in most eagerly-evaluated languages) requires some
mutation, although objects are never mutated after construction, and
Cliff had implemented hash-consing manually, rather than leaving it up
to the implementation. Thus we might instead need to distinguish
between \emph{automatic identity management}\footnote{Akin to a
  programmer needing to specify the lifetimes of objects somehow with
  \emph{manual memory management}, a programmer needs to provide
  \emph{aliasing} information, if an object can be reused or needs to
  be freshly allocated, and any possible mutations as part of an
  interface, when using manual identity management.} and
\emph{manual identity management}, instead of immutable and
mutable objects; only manual identity management is reasonable for
mutable objects, but either is usable with immutable objects.
