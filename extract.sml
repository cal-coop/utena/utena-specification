(* We use a simple state machine to determine what to emit for LaTeX. *)
datatype state = Start | Prose | Code
val startCode = "\n\\begin{minipage}{\\textwidth}\\begin{lstlisting}"
val endCode = "\\end{lstlisting}\\end{minipage}"

fun transition Start "(**\n" = ("", Prose)
  | transition Start l = (startCode ^ "\n" ^ l, Code)
  | transition Prose "**)\n" = (startCode ^ "\n", Code)
  | transition Prose l = (l, Prose)
  | transition Code "(**\n" = (endCode ^ "\n", Prose)
  | transition Code l = (l, Code)
fun finish Start = ""
  | finish Prose = ""
  | finish Code = endCode ^ "\n"

fun process input output state =
    case TextIO.inputLine input of
        SOME l =>
        let val (text, state') = transition state l in
            TextIO.output (output, text);
            process input output state'
        end
      | NONE => TextIO.output (output, finish state);

fun extract input_name output_name =
    let val input = TextIO.openIn input_name
        val output = TextIO.openOut output_name in
        process input output Start;
        TextIO.closeIn input;
        TextIO.closeOut output
    end

val () = extract "semantics.sml" "generated-semantics.tex"
val () = OS.Process.exit OS.Process.success
