(**
\section{Data structures}

We use a \texttt{nat}ural type for non-negative integers,
where a count of things is asked for. This type is equivalent to
\texttt{int}, so non-negative-ness is not checked either statically
or dynamically.
**)
type nat = int
(**
``Names'' are represented using the \texttt{obj info} type, which
contains an object to use as a name, and the accessibility of the name.
**)
datatype access = Private | Public
type 'a info = 'a * access

(**
Dynamic environments and dictionaries are represented as
\emph{association lists} (lists of key-value pairs), which we
define extending and lookup for.
**)
type 'a alist = ('a * 'a) list
fun lookup (key : 'a) (l : 'a alist) (test : 'a * 'a -> bool) : 'a option =
    case List.find (fn (k, _) => test (k, key)) l of
        SOME (_, v) => SOME v
      | NONE        => NONE
fun extend (key : 'a) (value : 'a) (l : 'a alist) : 'a alist =
    (key, value) :: l
(**
There are some peculiarities with the objects manipulated by
the Utena abstract machine.
The machine needs to manipulate symbols, so symbols are
made primitive. Slots of an object may be
unbound temporarily during initialisation. Escape
continuations have identity, so we need a spurious ref-cell
in ML to get identity.
**)
datatype obj = Symbol of string
             | Object of class * obj option array
             | Class of class
             | Escape of ar ref
     and class = Klass of obj option * class_definition
(**
Utena methods, primitive methods, and constructors for classes all
provide a parameter list for reflection. But Utena methods access
arguments through a lexical environment, whereas primitive methods
access arguments through an argument list, and each initialisation
method for each slot may provide its own parameter list while
constructing a class.
Self methods have no argument lists, as they cannot accept arguments,
so their argument lists must be empty.
**)
     and class_definition = Definition of {methods : method list,
                                           slots : slot list,
                                           mutable : bool,
                                           constructor_name : obj,
                                           constructor_params : obj list}
     and method = Method of {info : obj info,
                             insts : instruction list,
                             parameters : obj list,
                             recursive : bool}
                | SelfMethod of obj info
                | PrimitiveMethod of {info : obj info,
                                      parameters : obj list,
                                      function : primitive_function}
     and primitive_function = Primitive of obj * ar * obj list *
                                           thread_state * dyn_env -> state
(**
A slot may have one or two names. The first name is a \emph{reader} method
which returns the value of the slot, and the second name is a \emph{writer}
method which sets the value of the slot. A slot also has a method which is activated
in order to compute the initial value of the slot.
**)
     and slot = Slot of {reader : obj info, writer : obj info option,
                         init : method}
(**
A thread in Utena stores its own \emph{dictionary} associating
keys with values. The dictionary is used by our platform to store the default
values of dynamic variables, as the default values are thread-local and lazily
computed. The dictionary is similar to the \emph{process dictionary} in Erlang, but
this usage of the dictionary prevents a key from being written twice to one
dictionary.
**)
     and thread_state = Thread of {dictionary : obj alist ref}
(**
The Utena abstract machine keeps stepping through activation records,
until it terminates successfully (with a \texttt{Done} state) or
crashes (with a \texttt{Crash} state).
**)
     and state = Continue of ar | Crash of string | Done of obj
     and ar = Bottom
(**
The machine executes methods using five registers: a list of instructions,
an environment object, a data stack, a return stack and a dynamic environment.
**)
            | AR of instruction list * obj * obj list * ar * dyn_env
(**
The machine constructs an object by walking the list of slots
in the class definition of the object. Note that \texttt{target} is
always an \texttt{Object}; but it would require some duplication to
enforce that with the ML type system (or refinement types, which we
don't have).
**)
            | Construct of {target : (* Object *) obj,
                            remaining : slot list, index : nat,
                            next : ar, dynenv : dyn_env,
                            arguments : obj list}
     and instruction = Drop
                     | Literal of obj
                     | MakeClass of class_definition
                     | TargetSend of nat * bool
                     | SelfSend of nat * bool
(**
The dynamic environment also contains a list of valid escape
continuations. An implementation does not need to maintain
this list, but the list is necessary to check when continuations
are valid, i.e. when unwinding is defined.
**)
     and dyn_env = DynEnv of obj list * obj
(**
Reference equality is defined by \texttt{eq}; it arbitrarily
returns true or false if an attempt to determine the equality
of two immutable objects is made. The function \texttt{arbitrarily}
should really return either of its arguments (as inspired by the
semantics presented in \cite{compiling-with-continuations}).
**)
fun arbitrarily (a : 'a) (b : 'a) : 'a = a

fun eq (a : obj, b : obj) : bool =
    case (a, b) of
        (Class _,        Class _)        => arbitrarily true false
      | (Symbol a,       Symbol b)       => a = b
      | (Escape a,       Escape b)       => a = b
      | (Object (ak, a), Object (bk, b)) =>
        let val Klass (_, Definition aDef) = ak
            val Klass (_, Definition bDef) = bk in
            if         (not (#mutable aDef))
               andalso (not (#mutable bDef))
               andalso a = b
            then arbitrarily true false
            else a = b
        end
      | (_,              _)              => false
(**
The abstract machine may also raise \texttt{InvalidContination}
if an attempt to unwind from an escape continuation which is
no longer ``on the stack''. The consequences are undefined
if the abstract machine raises this exception.
**)
exception InvalidContinuation
(**
\section{Instruction validation}
\label{section:semantics:validation}

The validator is described more formally here as the function
\texttt{validate}. The function \texttt{effects} provides
the input count, output count, and if it is necessary for
an instruction to be in tail position, identically to
Table~\ref{table:validation}.
**)
fun effects Drop                = (1,     0, false)
  | effects (Literal _)         = (0,     1, false)
  | effects (MakeClass _)       = (0,     1, false)
  | effects (TargetSend (n, t)) = (n + 2, 1, t)
  | effects (SelfSend   (n, t)) = (n + 1, 1, t)

fun validate (instructions : instruction list) : bool =
    let fun implies x y = (not x) orelse y
        fun step []        1 = true
          | step []        _ = false
          | step (i::rest) n =
            let val (ins, outs, tail) = effects i in
                        ins <= n
                andalso implies tail (rest = [])
                andalso step rest (n - ins + outs)
            end in
        step instructions 0
    end

(**
\pagebreak
\section{Evaluation}

We first define how to restore an activation record, when its callee
returns a value. Execution finishes when we run out of activation
records.
**)
fun restore (Bottom : ar) (value : obj) : state = Done value
(**
The return value is pushed onto the data stack before continuing
to run instructions.
**)
  | restore (AR (code, env, data, next, dynenv)) v =
    Continue (AR (code, env, v :: data, next, dynenv))
(**
When we return to a \texttt{Construct} record, we store the value
of the slot we just initialized, and move to the next slot.
**)
  | restore (Construct {target, remaining, index, next, dynenv, arguments}) v =
    (* This pattern is non-exhaustive to ML, but it shouldn't fail by
       definition of Construct. *)
    let val Object (_, a) = target in
        Array.update (a, index, SOME v);
        Continue (Construct {target=target, remaining=remaining,
                             index=index + 1, next=next,
                             dynenv=dynenv, arguments=arguments})
    end

(**
Now we can define how to step through activation records. We'll start
with how to step through a method. We implicitly return after running out
of instructions in a code method. We must have one value on the data stack to
use as the return value at this point, due to prior validation of the method.
**)
fun step (t : thread_state) (AR (code, env, data, next, dynenv)) : state =
    (case code of
         [] => restore next (hd data)
(**
\texttt{Drop} and \texttt{Literal} only update the data stack.
**)
       | Drop      :: rest => Continue (AR (rest, env, tl data,   next, dynenv))
       | Literal l :: rest => Continue (AR (rest, env, l :: data, next, dynenv))
(**
\texttt{MakeClass} creates a class from the class definition associated
with the instruction.
**)
       | MakeClass d :: rest =>
         let val c = Class (Klass (SOME env, d)) in
             Continue (AR (rest, env, c :: data, next, dynenv))
         end
(**
\texttt{TargetSend} and \texttt{SelfSend} have to pick the
right values from the data stack, then perform a message send.
Note that we must reverse the argument list, as the last argument
was pushed last, and thus will be the first element of the stack.

Destructuring the stack as we do won't go wrong, due to validation.
**)
       | TargetSend (count, tail) :: rest =>
         let val arguments = rev (List.take (data, count))
             val receiver :: name :: data' = List.drop (data, count) in
             send name receiver arguments dynenv Public
                  (tailOf tail (AR (rest, env, data', next, dynenv))) t
         end
       | SelfSend (count, tail) :: rest =>
         let val arguments = rev (List.take (data, count))
             val name :: data' = List.drop (data, count) in
             send name env arguments dynenv Private
                  (tailOf tail (AR (rest, env, data', next, dynenv))) t
         end)
(**
If we've initialised all the slots of an instance, we return the
instance to the caller.
**)
  | step t (Construct {target, remaining=[], index, next, dynenv, arguments}) =
    restore next target
(**
If there are more slots to initialise, activate the initialisation
method for the first slot.
**)
  | step t (Construct {target, remaining=(Slot s) :: rest, index,
                       next, dynenv, arguments}) =
    activate target arguments dynenv
             (Construct {target=target, remaining=rest, index=index,
                         next=next, dynenv=dynenv, arguments=arguments})
             (#init s) t
(**
We perform a tail-send by skipping over the most recent activation
record. This activation record has to be \texttt{AR} as neither
object construction nor starting a thread perform a tail call.
**)
and tailOf false (a : ar) : ar = a
  | tailOf true (AR (_, _, _, next, _)) = next
(**
Now we define how a message is sent.
**)
and send (name : obj) (receiver : obj) (arguments : obj list)
         (dynenv : dyn_env) (a : access) (next : ar) (t : thread_state) : state =
    case receiver of
(**
A class only understands its constructor.
**)
        Class (k as Klass (_, Definition d)) =>
        (case (eq (#constructor_name d, name),
               length arguments = length (#constructor_params d)) of
             (true,  true) =>
             Continue (Construct {target=receiver, remaining=(#slots d),
                                  index=0, next=next,
                                  dynenv=dynenv, arguments=arguments})
           | (false, _)     => Crash "message not understood"
           | (_,     false) => Crash "argument count mismatch")
(**
An object searches its class for a method to use. If there is no such
method, and the message has \texttt{Private} access (due to being
a self-send), the object the passes on the search to its enclosing object.
**)
      | Object (k as Klass (enc, _), _) =>
        (case (findMethod k name a, enc, a) of
             (SOME m, _,        _) =>
             activate receiver arguments dynenv next m t
           | (NONE,   SOME enc, Private) =>
             send name enc arguments dynenv a next t
           | (NONE,   _,        _) => Crash "message not understood")
(**
Symbols don't understand any messages in this core; they only have
identity.
**)
      | Symbol _ => Crash "message not understood"
(**
Escape continuations can only be unwound. An escape continuation is
only valid during the extent of the \texttt{call/raw-ec} send which
created the escape continuation.
**)
      | Escape e =>
        let val DynEnv (ecs, _) = dynenv in
            case (eq (Symbol "unwind", name),
                  1 = length arguments,
                  List.find (fn other => eq (other, receiver)) ecs) of
                (true, true, SOME _) => restore (!e) (hd arguments)
              | (false, _, _) => Crash "message not understood"
              | (_, false, _) => Crash "argument count mismatch"
              | (_, _, NONE) => raise InvalidContinuation
        end
(**
\section{Method activation}
**)
and activate (receiver : obj) (arguments : obj list) (dynenv : dyn_env)
             (next : ar) (m : method) (t : thread_state) : state =
    let fun enclosingObject (Object (Klass (p, _), _)) : obj option = p in
        case m of
(**
Activating a code method requires us to create a synthetic instance
to use as the ``environment'', and an \texttt{AR} with the
instructions of the method and the environment.
**)
            Method {info, insts, parameters, recursive} =>
            if length arguments = length parameters
            then let val parent = if recursive
                                  then SOME receiver
                                  else enclosingObject receiver
                     val env = syntheticInstance parent parameters arguments in
                     Continue (AR (insts, env, [], next, dynenv))
                 end
            else Crash "argument count mismatch"
          | SelfMethod _ => restore next receiver
(**
We just call the primitive function when activating a primitive method.
**)
          | PrimitiveMethod {info, parameters, function=Primitive f} =>
            if length arguments = length parameters
            then f (receiver, next, arguments, t, dynenv)
            else Crash "argument count mismatch"
    end
(**
We generate a \emph{synthetic class} for each method on each object, and
an instance of that class to store arguments to the method.

We'll never actually use the constructor of a synthetic class, but we
 still have to fill in how the constructor would work. So we provide
a primitive method which crashes the machine.
**)
and syntheticSlotConstructor _ =
    Crash "attempted to construct a slot of a synthetic class"
and syntheticInstance (receiver : obj option) (parameters : obj list)
                      (arguments : obj list) : obj =
    let val bogusPrimitive =
            PrimitiveMethod {info=(Symbol "constructor", Private),
                             parameters=[],
                             function=Primitive syntheticSlotConstructor}
(**
We synthesise an immutable class with an immutable private slot for
each parameter.
**)
        val slots = map (fn p => Slot {reader=(p, Private),
                                       writer=NONE,
                                       init=bogusPrimitive})
                        parameters
        val definition = Definition {methods=[], mutable=false,
                                     constructor_name=Symbol "activation-record",
                                     constructor_params=[],
                                     slots=slots}
        val syntheticClass = Klass (receiver, definition) in
(**
We can finally construct the instance by copying the argument list into
the storage of the instance, as we preserved the order of arguments.
**)
    Object (syntheticClass, Array.fromList (map SOME arguments))
    end
and infoOf (Method m) : obj info = #info m
  | infoOf (SelfMethod i)        = i
  | infoOf (PrimitiveMethod m)   = #info m
and findMethod (Klass (_, Definition d)) (name : obj) (a : access) =
(**
We ignore methods with inappropriate accessibility. Only a private
method is inappropriate for a public (i.e. targeted) send with
our options for accessibility.
**)
    let fun compatible (found : access) =
            case (a, found) of
                (Public, Private)  => false
              | (_,      _)        => true
        val method = List.find (fn m =>
                                   let val (n, a) = infoOf m in
                                               eq (n, name)
                                       andalso compatible a
                                   end)
                               (#methods d) in
        case method of
            SOME m => method
(**
If we find a relevant reader or writer for a slot, we produce a
synthetic method which performs the appropriate action on an instance.
We first associate slots with indices into storage to perform the search.
**)
          | NONE =>
            let val indices = List.tabulate (length (#slots d), fn x => x)
                val pairs = ListPair.zip (indices, #slots d)
                fun relevant NONE = false
                  | relevant (SOME (n, a)) = eq (n, name) andalso compatible a
(**
The primitive methods read and write slots of the target. The reader
crashes if it encounters an unbound slot.

Destructuring the argument lists as we do will not go wrong, due to
\texttt{activate} refusing to activate a method with the wrong
 number of arguments.
**)
                fun reader (i : nat) =
                    Primitive (fn (Object (_, a), r, [], _, _) =>
                                  case Array.sub (a, i) of
                                      SOME e => restore r e
                                    | NONE   => Crash "slot unbound")
                fun writer (i : nat) =
                    Primitive (fn (Object (_, a), r, [v], _, _) =>
                                  (Array.update (a, i, SOME v);
                                   restore r v)) in
                case (List.find (fn (_, Slot s) =>
                                    relevant (SOME (#reader s))) pairs,
                      List.find (fn (_, Slot s) =>
                                    relevant       (#writer s))  pairs) of
                    (SOME (i, Slot s), _) =>
                    SOME (PrimitiveMethod {info=(#reader s),
                                           parameters=[],
                                           function=reader i})
                  | (NONE,             SOME (i, Slot s)) =>
                    (* Option.valOf can't go wrong here, as there has to
                       be a writer in order for the slot to be relevant. *)
                    SOME (PrimitiveMethod {info=Option.valOf (#writer s),
                                           parameters=[Symbol "value"],
                                           function=writer i})
                  | (NONE,             NONE) => NONE
            end
    end
(**
\section{Toplevel}

We finally need to initialise and step the abstract machine somehow. To
execute a method we create a class with the method, and an instance of
the class.
**)
fun machine (m : method) (initialDynEnv : obj) : state =
    let val (name, _) = infoOf m
        val class = (Klass (NONE,
                            Definition {methods=[m], slots=[],
                                        mutable=false,
                                        constructor_name=Symbol "toplevel",
                                        constructor_params=[]}))
        val instance = Object (class, Array.array (0, NONE)) in
        activate instance [] (DynEnv ([], initialDynEnv))
                 Bottom m (Thread {dictionary=ref []})
    end
(**
The machine can be stepped in a loop until termination.
**)
fun run (s : state) : state =
    let val thread = (Thread {dictionary=ref []})
        fun loop (Continue a) = loop (step thread a)
          | loop done         = done in
        loop s
    end
(**
\section{Primitives}

Several primitive methods are exposed to the user, to allow for
implementing non-local control flow and dynamic binding. We
define a function \texttt{primitive} to provide some brevity
in defining these primitive methods.
**)
fun primitive (name : string) (parameters : string list)
              f : method =
    PrimitiveMethod {info=(Symbol name, Public),
                     parameters=map Symbol parameters,
                     function=Primitive f}
val vmMethods : method list = [
(**
\texttt{call/raw-ec} provides an \texttt{Escape} continuation
object to a function. The object is ``raw'' in that it does not
check if the continuation is still valid; user code must prevent
an invalid continuation from being unwound. (See the appendix
on how validity may be tracked by Utena code.)
**)
    primitive "call/raw-ec" ["f"]
              (fn (_, r, [f], t, DynEnv (ecs, env)) =>
                  let val ec = Escape (ref r) in
                      send (Symbol "value") f [ec]
                           (DynEnv (ec :: ecs, env))
                           Public r t
                  end),
(**
\texttt{call-with-dynamic-environment} installs a new
dynamic environment for the extent of sending \texttt{value}
to a function. \texttt{dynamic-environment} retrieves the environment.
**)
    primitive "call-with-dynamic-environment" ["f", "env"]
              (fn (_, r, [f, env], t, DynEnv (ecs, _)) =>
                  send (Symbol "value") f []
                       (DynEnv (ecs, env))
                       Public r t),
    primitive "dynamic-environment" []
              (fn (_, r, [], t, DynEnv (_, env)) =>
                  restore r env),
(**
\texttt{dictionary-put!} and \texttt{lookup-dictionary} are
similar, but operate on the dictionary of the thread, and
\texttt{dictionary-put!} performs an update that is not localised
to any dynamic extent.
**)
    primitive "dictionary-put!" ["k", "v"]
              (fn (_, r, [k, v], Thread {dictionary}, d) =>
                  (dictionary := extend k v (!dictionary);
                   restore r v)),
    primitive "lookup-dictionary" ["k", "default"]
              (fn (_, r, [k, default], t as Thread {dictionary}, d) =>
                  case lookup k (!dictionary) eq of
                      SOME v => restore r v
                    | NONE   => send (Symbol "value") default
                                     [] d Public r t)
]
(**
These methods are exposed through the VM primitives module, which
may be supplied to user code. The user code can then provide its own
abstractions and encapsulate the primitives module.
**)
val vm : obj =
    Object (Klass (NONE,
                   Definition {methods=vmMethods,
                               slots=[],
                               mutable=false,
                               constructor_name=Symbol "vm-class",
                               constructor_params=[]}),
            Array.array (0, NONE))
