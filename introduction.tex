\chapter{Introduction}

\epigraph{
  Specialization of machines in terms of end product requires that the
  machine be thrown away when the product is no longer needed. Yet the
  work the production machine does can be reduced to a set of basic
  functions -- forming, holding, cutting, and so on -- and these
  functions, if correctly analyzed, can be packaged and applied to
  operate on a part as needed.}{\cite{machines}}

\section{Maximalism}
\label{section:introduction:maximalism}

Utena is a system that wants to empower an individual developer
to achieve as much as possible with as little effort as possible.
This principle of \emph{empowerment} can be realised
through techniques like program generation, introspection, meta-media,
automatic memory management, and interactive debugging. This is
not the same as dangerous and downright unsafe capabilities that
could be considered to be ``empowering''. such as access to raw
memory.

This is because we believe that empowering individual
developers to have more leverage, and do more things with their
system that otherwise would not be possible, allows them to
empower other people to do the same, without those people needing
to be in the profession of programming. This is a necessary step
to break the hierarchy between producers and consumers of software.

We use the term \emph{maximalism} to distance ourselves from
so-called minimalists, who state the equation ``less is more'', but
fail to demonstrate it. There is a fine line to tread in what we actually
\emph{simplify}; to have simplified any software, we must provide
sufficient functionality in the software, else one will have to use more
software to achieve their aims, neutralising any gains in
simplicity\footnote{Neutralising at best; interoperability and interfacing
  between more software systems may require more code and more
  cognitive overhead \cite{monolingual}.}.
To achieve this aim, we must carefully analyse how to express a
wide range of functionality in a simple meta-medium.

\section{Message-based programming \& Modularity}
\label{section:introduction:message-based-programming}

Newspeak introduces the term \emph{message-based programming} to refer
to a style of programming where all names are late bound and there is
no global namespace \cite{bracha2010:modules-as-objects}.  Similarly
to Newspeak, Utena requires that all operations be expressed via
messages and all message selectors be late bound. Therefore all
dependencies are always mediated by an interface that is the set of
message selectors the client is able to send to that dependency.  In a
step further, Utena provides a syntax agnostic platform for programs
to run upon by embedding this principle into the virtual machine; any
machine byte-code must not depend on the implementation of any
primitive object.
